inc = {type:'INC', payload:1};
dec = {type:'DEC', payload:1};
reset = {type:'RESET'};

addTodo = todo => ({type:'ADD_TODO', payload: todo });

[inc,inc,addTodo('zrob zakupy'),dec,inc].reduce( (state, action) =>{
    switch(action.type){
        case 'INC': return {...state, sum: state.sum + action.payload }
        case 'DEC': return {...state, sum: state.sum - action.payload }
        case 'RESET': return {...state, sum: 0 }
        case 'ADD_TODO': return {...state, todos:[...state.todos, action.payload]  }
        default: return state
    }
},{
    placki:123,
    todos:[],
    sum:0
})
{placki: 123, todos: Array(1), sum: 2}placki: 123sum: 2todos: ["zrob zakupy"]__proto__: Object