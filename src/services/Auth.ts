
interface AuthConfig {
  url: string,
  client_id: string,
  response_type: 'token' | 'code',
  redirect_uri: string,
  scope: string[],
  show_dialog: boolean
}


export class Auth {
  token: string | null = null

  constructor(protected config: AuthConfig) { }

  logout() {
    this.token = null;
    sessionStorage.removeItem('token')
  }

  autoLogin() {
    if (!this.token) { this.extractToken() }
    if (!this.token) { this.authorize() }
  }

  getToken() {
    return this.token
  }

  extractToken() {
    let token = new URLSearchParams(window.location.hash).get('#access_token')

    const jsonToken = window.sessionStorage.getItem('token')
    if (jsonToken && !token) { token = JSON.parse(jsonToken) }

    if (token) {
      window.location.hash = ''
      this.token = token
      window.sessionStorage.setItem('token', JSON.stringify(this.token))
    }
  }

  authorize() {
    sessionStorage.removeItem('token')
    const {
      url,
      client_id,
      response_type,
      redirect_uri,
      scope,
      show_dialog } = this.config

    const params = new URLSearchParams({
      client_id,
      response_type,
      redirect_uri,
      show_dialog: show_dialog.toString(),
      scope: scope.join(' ')
    })

    // window.open(`${url}?${params}`,'_blank','width=100')
    // window.onmessage = (token) { ... 

    // accept_token.html:
    // window.opener.postMessage({... token }); window.close()

    window.location.href = (`${url}?${params}`)
  }

}