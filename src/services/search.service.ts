import { AlbumsSearchResponse, TracksSearchResponse, Album, Track, PagingObject } from "../models/Album"
import { auth } from "../App"
import axios from 'axios'
import { Auth } from "./Auth"
import { Playlist, PlaylistPayload } from "../models/Playlist"

axios.interceptors.request.use(config => {
  config.headers['Authorization'] = `Bearer ` + auth.getToken()
  return config
})
axios.interceptors.response.use(resp => resp, error => {
  if (error.isAxiosError) {
    if (error.response.status === 401) {
      auth.authorize()
    }
    if (error.response.data?.error) {
      throw Error(error.response.data?.error.message)
    }
  }
  return error
})

export const fetchAlbumById = (album_id: string) => {
  return axios.get<Album>(`https://api.spotify.com/v1/albums/${album_id}`, {
  }).then(resp => resp.data)
}

export const searchAlbums = (params: { query: string }) => {
  return axios.get<AlbumsSearchResponse>('https://api.spotify.com/v1/search', {
    params: { ...params, type: 'album' },
  }).then(resp => resp.data.albums.items)
}

export const searchTracks = (params: { query: string }) => {
  return axios.get<TracksSearchResponse>('https://api.spotify.com/v1/search', {
    params: { ...params, type: 'track' },
  }).then(resp => resp.data.tracks.items)
}


export const fetchPlaylists = () => {
  return axios.get<PagingObject<Playlist>>(`https://api.spotify.com/v1/me/playlists`, {
  }).then(resp => resp.data.items)
}

export const fetchPlaylistById = (id: string) => {
  return axios.get<Playlist>(`https://api.spotify.com/v1/playlists/${id}`, {
  }).then(resp => resp.data)
}

export const updatePlaylistREquest = (
  id: string,
  playlist: PlaylistPayload
) => axios.put<Playlist>(
  `https://api.spotify.com/v1/playlists/${id}`,
  playlist,
).then(resp => resp.data)




export const createPlaylistREquest = (params: {
  user_id: string,
  playlist: PlaylistPayload
}) => {
  return axios.post<Playlist>(
    `https://api.spotify.com/v1/users/${params.user_id}/playlists`,
    params.playlist,
    {}
  ).then(resp => resp.data)
}

export const addTrackToPlaylistRequest = (
  trackUri: string, playlistId: string
) => {
  return axios.post<Playlist>(
    `https://api.spotify.com/v1/playlists/${playlistId}/tracks`,
    null, {
    params: {
      uris: [trackUri].join(',')
    }
  }
  ).then(resp => resp.data)
}



export const searchByType = (params: {
  query: string, type: string
}): Promise<Album[] | Track[]> => {
  switch (params.type) {
    case 'album': return searchAlbums(params)
    case 'track': return searchTracks(params)
    default: throw 'Invalid search type'
  }
}