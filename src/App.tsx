import React, { Suspense } from 'react';
// import logo from './logo.svg';
// import styles from './App.module.scss';
import 'bootstrap/dist/css/bootstrap.css'
import { Auth } from './services/Auth';
import { ManagePlaylistProvider } from './contexts/ManagePlaylists';
import { UserContextProvider } from './contexts/UserContext';
import { NavBar } from './components/NavBar';
import { Route, Switch, Redirect } from 'react-router-dom'
import { store, history } from './store'
import PlaylistsRedux from './views/PlaylistsRedux';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
// const routing = {
//   home: { path: '/' }
// }

// const AboutLazy = React.lazy(() => import('./views/' + i + 'About'))
const AboutLazy = React.lazy(() => import('./views/About'))
const SearchLazy = React.lazy(() => import('./views/Search'))
const AlbumDetailsLazy = React.lazy(() => import('./views/AlbumDetails'))
const PlaylistsReduxLazy = React.lazy(() => import('./views/PlaylistsRedux'))


function RenderLazy<P>(Component: React.ComponentType<P>) {
  return (props: P) => <Suspense fallback={<h1>Loading...</h1>}>
    <Component {...props} />
  </Suspense>
}

function App() {

  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <UserContextProvider>
          <ManagePlaylistProvider>
            <div>
              <NavBar />
              <div className="container">
                <div className="row">
                  <div className="col">

                    <Switch>
                      <Redirect from="/" exact={true} to="search" />
                      <Route path="/playlists" component={RenderLazy(PlaylistsReduxLazy)} />
                      <Route path="/search" component={RenderLazy(SearchLazy)} />
                      <Route path="/albums/:album_id" component={RenderLazy(AlbumDetailsLazy)} />
                      <Route path="/about" component={RenderLazy(AboutLazy)} />
                      <Route path="*" render={() => <h2>Page Not Found</h2>} />
                    </Switch>


                  </div>
                </div>
              </div>
            </div>
          </ManagePlaylistProvider>
        </UserContextProvider>
      </ConnectedRouter>
    </Provider>
  );
}

export default App;



export const auth = new Auth({
  url: 'https://accounts.spotify.com/authorize',
  client_id: '336db8fcd75a49ccb0b6d6d4d902793b',
  redirect_uri: 'http://localhost:3000/',
  response_type: 'token',
  scope: [
    'streaming',
    'playlist-modify-private',
    'playlist-read-private'
  ],
  show_dialog: true,
})

auth.autoLogin()
