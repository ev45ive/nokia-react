import { PagingObject, Track } from "./Album";

export interface Entity {
  // id?: number | string
  id: string
  name: string
}

export interface Playlist<T = Track> extends Entity {
  type: 'Playlist'
  favorite: boolean;
  color: string;
  tracks?: PagingObject<{track:T}>
  collaborative?: boolean;
  public?: boolean;
  description?: string;
}

export interface PlaylistPayload {
  name: string;
  collaborative: boolean;
  public: boolean;
  description: string;
}


// const p: Playlist<string> = {
//   type: 'Playlist',
//   id: 123,
//   name: 'Test',
//   favorite: true,
//   color: 'red',
//   tracks:['123']
// }

// const e: Playlist | Track = {}

// switch(e.type){
//   case 'Playlist': e.tracks; break;
//   case 'Track': e.duration; break;
// }

// p.tracks?.length
// if('string' === typeof p.id ){
//   p.id 
// }else if(p.id){
//   p.id 
// }


// interface Vector { x: number; y: number, length: number }
// interface Point { x: number; y: number }

// let v: Vector = { x: 12, y: 3, length: 12 }
// let p: Point = { x: 12, y: 3 }

// v = p
// p = v 
