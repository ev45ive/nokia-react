// Generated by https://quicktype.io

export interface AlbumsSearchResponse {
  albums: PagingObject<Album>;
}
export interface TracksSearchResponse {
  tracks: PagingObject<Track>;
}

export interface PagingObject<T> {
  href: string;
  items: T[];
  limit: number;
  next: string;
  offset: number;
  previous: null;
  total: number;
}

export interface Album {
  id: string;
  name: string;
  images: Image[];
  album_type: AlbumTypeEnum;
  artists: Artist[];
  available_markets: string[];
  external_urls: ExternalUrls;
  href: string;
  release_date: string;
  release_date_precision: ReleaseDatePrecision;
  total_tracks: number;
  type: AlbumTypeEnum;
  uri: string;
  tracks?: PagingObject<Track>
}

export enum AlbumTypeEnum {
  Album = "album",
  Compilation = "compilation",
  Single = "single",
}

export interface Artist {
  id: string;
  name: string;
  external_urls: ExternalUrls;
  href: string;
  type: ArtistType;
  uri: string;
}

export interface ExternalUrls {
  spotify: string;
}

export enum ArtistType {
  Artist = "artist",
}

export interface Image {
  height: number;
  url: string;
  width: number;
}

export enum ReleaseDatePrecision {
  Day = "day",
}

export interface Track {
  album: Album;
  artists: Artist[];
  available_markets: string[];
  disc_number: number;
  /**
   * Duration in miliseconds
   */
  duration_ms: number;
  explicit: boolean;
  external_ids: Externalids;
  external_urls: Externalurls;
  href: string;
  id: string;
  is_local: boolean;
  name: string;
  popularity: number;
  /**
   * Link to MP3 sample
   */
  preview_url: string;
  track_number: number;
  type: string;
  uri: string;
}

export interface Externalids {
  isrc: string;
}

export interface Externalurls {
  spotify: string;
}