import React, { FC, useReducer, Reducer, useCallback } from "react";
import { Playlist } from "../models/Playlist";
import { Track } from "../models/Album";

interface C {
  selected: Playlist | null,
  addTrack(track: Track): void
  removeTrack(trackId: Track['id']): void
  setSelected(selected: Playlist): void
}

const NoContextError = () => {
  throw 'No Context for ManagePlaylists Provided'
}

export const ManagePlaylists = React.createContext<C>({
  selected: null,
  addTrack: NoContextError,
  removeTrack: NoContextError,
  setSelected: NoContextError,
})


export const ManagePlaylistProvider: FC<{}> = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, {
    selected: null
  })

  const addTrack = useCallback((track: Track) => {
    dispatch({ type: 'ADD_TRACK', track })
  }, [])
  const removeTrack = () => { }
  const setSelected = () => { }

  return <ManagePlaylists.Provider value={{
    selected: state.selected,
    addTrack,
    removeTrack,
    setSelected,
  }}>
    {children}
  </ManagePlaylists.Provider>
}

type State = {
  selected: Playlist | null,
}
type Actions = any

const reducer: Reducer<State, Actions> = (state, action) => {
  switch (action.type) {
    case 'ADD_TRACK': return { ...state, selected: action.track }
  }
  return state
}