import { User } from "../models/User";
import React, { FC, useState, useEffect } from "react";
import { auth } from "../App";
import axios from 'axios'
import { useDispatch, useSelector } from "react-redux";
import { userLoggedIn, selectUser, userLoggedOut } from "../modules/auth/auth.reducer";


interface Ctx {
  user: User | null
  login(): void
  logout(): void
}

const NoContextError = () => {
  throw 'No UserContext Provided'
}

export const UserContext = React.createContext<Ctx>({
  user: null,
  login: NoContextError,
  logout: NoContextError
})


export const UserContextProvider: FC<{}> = ({ children }) => {

  const user = useSelector(selectUser)
  const dispatch = useDispatch()

  useEffect(() => {
    login()
  }, [])

  const login = () => {
    if (!auth.token) { return auth.authorize() }

    axios.get<User>('https://api.spotify.com/v1/me', {
      headers: { Authorization: 'Bearer ' + auth.getToken() }
    }).then(resp => {
      dispatch(userLoggedIn(resp.data))
    })
  }

  const logout = () => {
    dispatch(userLoggedOut())
    auth.logout()
  }

  return <UserContext.Provider value={{ user, login, logout }}>
    {children}
  </UserContext.Provider>
}
