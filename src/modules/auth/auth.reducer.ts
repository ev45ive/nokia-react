import { Reducer } from "redux"
import { User } from "../../models/User"

enum ActionTypes {
  USER_LOGGED_IN = 'USER_LOGGED_IN',
  USER_LOGGED_OUT = 'USER_LOGGED_OUT'
}

export const authReducer: Reducer = (state = {
  user: null
} as any, action) => {

  switch (action.type) {
    case ActionTypes.USER_LOGGED_IN: return { ...state, user: action.payload.user }
    case ActionTypes.USER_LOGGED_OUT: return { ...state, user: null }
    default: return state
  }

}

export const userLoggedOut = () => ({
  type: ActionTypes.USER_LOGGED_OUT
});

export const userLoggedIn = (user: User) => ({
  type: ActionTypes.USER_LOGGED_IN, payload: {
    user
  }
})


export const selectUser = (state: any) => state.auth.user