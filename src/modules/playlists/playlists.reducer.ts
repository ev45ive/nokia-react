import { Reducer, AnyAction, Action, ActionCreator, Dispatch } from "redux";
import { Playlist, PlaylistPayload } from "../../models/Playlist";
import { push, replace } from "connected-react-router";
import { ThunkAction } from 'redux-thunk'
import { AlbumTypeEnum, Track } from "../../models/Album";
import { createPlaylistREquest, fetchPlaylists as fetchPlaylistsRequest, fetchPlaylistById, updatePlaylistREquest, addTrackToPlaylistRequest } from "../../services/search.service";
import { selectUser } from "../auth/auth.reducer";

type State = {
  entities: Record<Playlist['id'], Playlist>,
  list: Playlist['id'][]
  // favorite: Playlist['id'][]
  // user_playlsits: Playlist['id'][]
  // recent: Playlist['id'][]
  selected?: Playlist['id'],
  loading: boolean
  error?: Error
}

// Flux Standard Action
// type, payload, meta?, error?

enum ActionTypes {
  PLAYLIST_LOADED = '[Playlists] Playlists loaded',
  PLAYLISTS_SELECTED = '[Playlists] Playlist selected',
  PLAYLIST_UPDATED = '[Playlists] Playlist updated',

  PLAYLIST_CREATE_START = '[Playlists] Playlist create start',
  PLAYLIST_CREATE_SUCCESS = '[Playlists] Playlist create success',
  PLAYLIST_CREATE_FAILED = '[Playlists] Playlist create failed',

  PLAYLIST_ADD_TRACK_START = '[Playlists] Playlist add track start',
  PLAYLIST_ADD_TRACK_SUCCESS = '[Playlists] Playlist add track success',
  PLAYLIST_ADD_TRACK_FAILED = '[Playlists] Playlist add track failed',

  PLAYLISTS_FETCH = '[Playlists] Playlists fetch',
}


interface PLAYLISTS_LOADED {
  type: ActionTypes.PLAYLIST_LOADED,
  payload: {
    data: Playlist[]
  }
}

interface PLAYLISTS_SELECTED extends Action<ActionTypes.PLAYLISTS_SELECTED> {
  payload: {
    id?: Playlist['id']
  }
}

interface PLAYLIST_UPDATED extends Action<ActionTypes.PLAYLIST_UPDATED> {
  payload: {
    data: Playlist
  }
}

interface PLAYLIST_CREATE_START extends Action<ActionTypes.PLAYLIST_CREATE_START> {
  payload: {
    data: PlaylistPayload
  }
}

interface PLAYLIST_CREATE_SUCCESS extends Action<ActionTypes.PLAYLIST_CREATE_SUCCESS> {
  payload: {
    data: Playlist
  }
}
interface PLAYLIST_CREATE_FAILED extends Action<ActionTypes.PLAYLIST_CREATE_FAILED> {
  payload: { error: Error }
}

interface PLAYLIST_ADD_TRACK_START extends Action<ActionTypes.PLAYLIST_ADD_TRACK_START> {
  payload: {
    trackId: Track['id']
    playlistId: Playlist['id']
  }
}

interface PLAYLIST_ADD_TRACK_SUCCESS extends Action<ActionTypes.PLAYLIST_ADD_TRACK_SUCCESS> {
  payload: {}
}
interface PLAYLIST_ADD_TRACK_FAILED extends Action<ActionTypes.PLAYLIST_ADD_TRACK_FAILED> {
  payload: { error: Error }
}

interface PLAYLISTS_FETCH extends Action<ActionTypes.PLAYLISTS_FETCH> {
  payload: {
    query: any,
    data: Playlist[],
    error: Error
  }
}

type Actions =
  | PLAYLISTS_LOADED
  | PLAYLISTS_SELECTED
  | PLAYLIST_UPDATED

  | PLAYLIST_CREATE_START
  | PLAYLIST_CREATE_SUCCESS
  | PLAYLIST_CREATE_FAILED

  | PLAYLIST_ADD_TRACK_START
  | PLAYLIST_ADD_TRACK_SUCCESS
  | PLAYLIST_ADD_TRACK_FAILED

  | PLAYLISTS_FETCH;

const initialState: State = {
  entities: {},
  list: [],
  selected: undefined,
  loading: false
}

/* Reducer  */
export const playlistsReducer: Reducer<State, Actions> = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.PLAYLIST_LOADED:
      return {
        ...state, list: action.payload.data.map(p => p.id),
        entities: action.payload.data.reduce((entities, p) => {
          entities[p.id] = p
          return entities
        }, state.entities)
      };
    /* === */
    case ActionTypes.PLAYLIST_CREATE_START: {
      return { ...state, loading: true }
    }
    case ActionTypes.PLAYLIST_CREATE_SUCCESS: {
      const p = action.payload.data
      return {
        ...state,
        loading: false,
        entities: { ...state.entities, [p.id]: p },
        list: [...state.list, p.id]
      }
    }
    case ActionTypes.PLAYLIST_CREATE_FAILED: {
      return { ...state, loading: false, error: action.payload.error }
    }
    /* === */
    case ActionTypes.PLAYLIST_ADD_TRACK_START: {
      return { ...state }
    }
    case ActionTypes.PLAYLIST_ADD_TRACK_SUCCESS: {
      const p = action.payload
      return { ...state, }
    }
    case ActionTypes.PLAYLIST_ADD_TRACK_FAILED: {
      return { ...state, error: action.payload.error }
    }
    /* === */

    case ActionTypes.PLAYLISTS_FETCH: {
      if (action.payload.query) {
        return { ...state, loading: true, error: undefined }
      }
      if (action.payload.data) {
        return {
          ...state, loading: false, list: action.payload.data.map(p => p.id),
          entities: action.payload.data.reduce((entities, p) => {
            entities[p.id] = p
            return entities
          }, state.entities)
        }
      }
      if (action.payload.error) {
        return { ...state, loading: false, error: action.payload.error }
      }
      return state
    }

    case ActionTypes.PLAYLISTS_SELECTED:
      return { ...state, selected: action.payload.id };
    case ActionTypes.PLAYLIST_UPDATED: {
      const draft = action.payload.data;
      return { ...state, entities: { ...state.entities, [draft.id]: draft } }
      // return { ...state, list: state.list.map(p => p.id == draft.id ? draft : p) };
    }
    default:
      return state
  }
}

/* Action Creators */

export const playlistsLoaded = (data: Playlist[]): PLAYLISTS_LOADED => {
  // validate data or throw
  return ({
    type: ActionTypes.PLAYLIST_LOADED,
    payload: {
      data
    }
  })
}

export const playlistSelected = (id?: Playlist['id']): PLAYLISTS_SELECTED => ({
  type: ActionTypes.PLAYLISTS_SELECTED,
  payload: {
    id
  }
})

export const playlistUpdated = (data: Playlist): PLAYLIST_UPDATED => ({
  type: ActionTypes.PLAYLIST_UPDATED,
  payload: {
    data
  }
})

export const playlistCreateStart: ActionCreator<PLAYLIST_CREATE_START> = (data: PlaylistPayload) => ({
  type: ActionTypes.PLAYLIST_CREATE_START,
  payload: { data }
})

export const playlistCreateSuccess: ActionCreator<PLAYLIST_CREATE_SUCCESS> = (data: Playlist) => ({
  type: ActionTypes.PLAYLIST_CREATE_SUCCESS,
  payload: { data }
})

export const playlistCreateFailed: ActionCreator<PLAYLIST_CREATE_FAILED> = (error: Error) => ({
  type: ActionTypes.PLAYLIST_CREATE_FAILED,
  payload: { error }
})

export const AddTrackToPlaylistStart: ActionCreator<PLAYLIST_ADD_TRACK_START> = (trackId, playlistId) => ({
  type: ActionTypes.PLAYLIST_ADD_TRACK_START,
  payload: { trackId, playlistId }
})

export const AddTrackToPlaylistSuccess: ActionCreator<PLAYLIST_ADD_TRACK_SUCCESS> = () => ({
  type: ActionTypes.PLAYLIST_ADD_TRACK_SUCCESS, payload: {}
})

export const AddTrackToPlaylistFailed: ActionCreator<PLAYLIST_ADD_TRACK_FAILED> = (error: Error) => ({
  type: ActionTypes.PLAYLIST_ADD_TRACK_FAILED, payload: { error }
})


/* Selectors */
// https://github.com/reduxjs/reselect
// https://github.com/paularmstrong/normalizr

export const playlistsSelectList = (state: any) => state.playlists.list.map(
  (id: number) => state.playlists.entities[id]);

export const playlistsSelectSelected = (state: any): Playlist | undefined => {
  // return state.playlists.list.find((p: Playlist) => p.id === state.playlists.selected)
  return state.playlists.entities[state.playlists.selected]
}


/* Async Action Creators */

export const fetchAndSelectPlaylist = (id: Playlist['id']) => async (dispatch: Dispatch) => {
  // fetch playlist
  const playlist = await fetchPlaylistById(id)

  dispatch(playlistUpdated(playlist))

  dispatch(playlistSelected(id))
  dispatch(push('/playlists/' + id))
}

export const playlistSaveChanges = (id: string, draft: PlaylistPayload) => async (dispatch: Dispatch) => {

  try {
    await updatePlaylistREquest(id, draft)
    const playlist = await fetchPlaylistById(id)

    dispatch(playlistUpdated(playlist))
  } catch (error) {
    // dispatch(playlistUpdateFailed(error))
  }

}

export const createPlaylist = (draft: PlaylistPayload) => async (dispatch: Dispatch, getState: any) => {

  dispatch(playlistCreateStart(draft))
  try {
    const { id } = selectUser(getState())
    const savedPlaylist = await createPlaylistREquest({ user_id: id, playlist: draft })
    dispatch(playlistCreateSuccess(savedPlaylist))

    dispatch(replace('/playlists/' + savedPlaylist.id))

  } catch (error) {
    dispatch(playlistCreateFailed(error))
  }

}


export const fetchPlaylists = () => async (dispatch: Dispatch, getState: any) => {

  try {
    dispatch({ type: ActionTypes.PLAYLISTS_FETCH, payload: { query: {} } })
    const data = await fetchPlaylistsRequest()

    dispatch({ type: ActionTypes.PLAYLISTS_FETCH, payload: { data } })

  } catch (error) {

    dispatch({ type: ActionTypes.PLAYLISTS_FETCH, payload: { error: [] } })
  }

}

export const addTrackToSelectedPlaylist = (track: Track) =>
  async (dispatch: Dispatch, getState: any) => {
    try {
      const playlist_id = playlistsSelectSelected(getState())?.id
      if (!playlist_id) { throw Error('No playlist selected') }

      dispatch(AddTrackToPlaylistStart())
      await addTrackToPlaylistRequest(track.uri, playlist_id)
      dispatch(AddTrackToPlaylistSuccess())

    } catch (error) {
      dispatch(AddTrackToPlaylistFailed(error))
    }

  }
