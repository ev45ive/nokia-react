import React, { useContext } from "react";
import { UserContext } from "../contexts/UserContext"
import { Route, Link, NavLink } from "react-router-dom";

export const NavBar: React.FC<{}> = () => {
  const { user, login, logout } = useContext(UserContext)

  return (<nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
    <div className="container">

      <NavLink className="navbar-brand"
        // isActive={(match, location) => { return match?.url.includes(...) true }}
        exact={true}
        activeClassName="active placki"
        to="/">Music App</NavLink>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav mr-auto">

          <li className="nav-item">
            <NavLink className="nav-link" to="/playlists">Playlists</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/search">Search</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/about">About</NavLink>
          </li>

        </ul>
        <ul className="navbar-nav">
          <li className="nav-item" ></li>

          <li className="navbar-text">
            {user?.display_name}
          </li>

          <li className="nav-item">
            {user ?
              <a className="nav-link" onClick={logout}>Logout</a>
              : <a className="nav-link" onClick={login}>Login</a>}
          </li>
          {/* {completed &&  <Redirect to="playlists" />} */}
        </ul>
        {/* <Route /> */}
      </div>
    </div>
  </nav >)
}