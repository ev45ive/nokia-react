import React, { useState, useEffect, useRef } from 'react'
import { Playlist } from '../models/Playlist'

interface Props {
  playlist: Playlist,
  onSave(draft: Playlist): void
  onCancel(): void
}

const PlaylistsForm = React.memo((props: Props) => {
  const selected = props.playlist
  const maxLength = 60

  const [name, setName] = useState(selected.name)
  const [favorite, setFavorite] = useState(selected.favorite)
  const [color, setColor] = useState(selected.color)
  const [isSaving, setIsSaving] = useState(false)

  useEffect(() => {
    setName(selected.name)
    setFavorite(selected.favorite)
    setColor(selected.color)
  }, [selected])

  useEffect(() => {
    if (!isSaving) { return }

    const handler = setTimeout(() => {
      setIsSaving(false)
    }, 2000)

    return () => clearTimeout(handler)
  }, [isSaving])


  const nameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    // name is valid?
    const value = event.target.value
    if (value.length < maxLength) {
      setName(event.target.value)
      // console.log(event.target.value, name)
    }
  }

  const save = () => {
    const draft: Playlist = {
      ...selected,
      name,
      favorite,
      color
    }
    isSaving || props.onSave(draft)
    setIsSaving(true)
  }

  const inputElemRef = useRef<HTMLInputElement>(null)

  useEffect(() => {
    inputElemRef.current?.focus()
  }, [])

  console.log('render')
  return (
    <div>
      {/* .form-group>label{Name:}+input.form-control */}
      <div className="form-group">
        <label>Name:</label>
        <input type="text" className="form-control" value={name} onChange={nameChange} ref={inputElemRef} />

        {maxLength - name.length} / {maxLength}
      </div>

      {/* .form-group>label{Favorite:}+input[type=checkbox] */}
      <div className="form-group">
        <label>Favorite:</label>
        <input type="checkbox" checked={favorite} onChange={event => setFavorite(event.target.checked)} />
      </div>

      {/* .form-group>label{Color:}+input[type=color] */}
      <div className="form-group">
        <label>Color:</label>
        <input type="color" value={selected.name} onChange={event => setColor(event.target.value)} />
      </div>

      <input type="button" value={isSaving ? 'Saving...' : "Save"} onClick={save} />
      <input type="button" value="Cancel" onClick={props.onCancel} />
    </div>
  )
})

export default PlaylistsForm
