import React, { useContext, useMemo } from 'react';
import { Track } from '../models/Album';
import { ManagePlaylists } from '../contexts/ManagePlaylists';

type P = {
  track: Track;
  onPlay?(track: Track): void
  onAddToPlaylist?(track: Track): void
};

export const TrackListItem = ({ track, onPlay, onAddToPlaylist }: P) => {
  const { addTrack } = useContext(ManagePlaylists)


  return useMemo(() => <div
    className="list-group-item" key={track.id}>

    {track.name}

    {/* <button onClick={() => addTrack(track)}>Add To Playlist</button> */}

    {/* {selected?.name} */}

    {/* <span className="float-right">{track.duration_ms}</span> */}
    <span className="float-right">
      {onAddToPlaylist && <button onClick={() => onAddToPlaylist(track)}>+</button>}
      {onPlay && <button onClick={() => onPlay(track)}>Play</button>}
    </span>
  </div>, [track, addTrack]);
}


// {/* <ManagePlaylists.Consumer>{
//   ({addTrack}) => <TrackListItem addTrack={addTrack}></TrackListItem>
// }</ManagePlaylists.Consumer> */}