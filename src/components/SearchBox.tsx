import React, { useState, useEffect, useRef } from 'react'

interface Props {
  query: string,
  onSearch(query: string): void
}

const SearchBox = ({ onSearch, query: parentQuery }: Props) => {
  const [query, setQuery] = useState(parentQuery)
  const isFirst = useRef(true)

  useEffect(() => {
    setQuery(parentQuery)
  }, [parentQuery])

  useEffect(() => {
    // Avoid effect on first render, unlock for next renders:
    if (isFirst.current) { isFirst.current = false; return; }

    const handler = setTimeout(() => {
      onSearch(query)
    }, 500)

    return () => clearTimeout(handler)
  }, [query])

  return (<div className="input-group mb-3">

    <input type="text" className="form-control" placeholder="Search"
      value={query}
      onChange={event => setQuery(event.target.value)} />

    <div className="input-group-append">
      <button className="btn btn-outline-secondary" onClick={() => onSearch(query)}>
        Search
      </button>
    </div>


  </div>
  )
}

export default SearchBox
