import React, { FC } from 'react'
import { Playlist } from '../models/Playlist'
import { TrackListItem } from './TrackListItem'

interface Props {
  playlist?: Playlist,
  onEditClicked?(): void
}

const PlaylistsDetails: FC<Props> = ({
  playlist,
  onEditClicked
}) => {

  return playlist ? (
    <div>
      <h3>{playlist.name}</h3>

      <dl>
        <dt>Favorite:</dt>
        <dd>{playlist.favorite ? 'Yes' : 'No'}</dd>

        <dt>Color:</dt>
        <dd style={{
          color: playlist.color,
          backgroundColor: playlist.color
        }}>{playlist.color}</dd>
      </dl>

      {onEditClicked &&
        <input type="button" value="Edit" onClick={onEditClicked} />}

      <div className="list-group mt-3">
        {
          playlist.tracks?.items?.map((item,index) =>
            <TrackListItem track={item.track} key={index} />
          )
        }
      </div>

    </div>
  ) : <p>Please select playlist</p>
}

export default PlaylistsDetails

// PlaylistsDetails.defaultProps = {
//   playlist: {
//     type: 'Playlist',
//     id: 123,
//     name: 'Test',
//     favorite: true,
//     color: '#ff00ff',
//   }
// }