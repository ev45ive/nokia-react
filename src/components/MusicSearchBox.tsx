import React, { useEffect, useRef, useReducer, useState } from 'react'

type QueryParams = {
  query: string
  type: string
}

interface Props {
  params: QueryParams
  onSearch(params: QueryParams): void
}

type State = {
  query: string
  type: string
}

enum ActionTypes {
  SET_QUERY = 'SET_QUERY',
  SET_TYPE = 'SET_TYPE',
  REPLACE_PARAMS = "REPLACE_PARAMS"
}

type ACTIONS =
  | SET_QUERY
  | SET_TYPE
  | REPLACE_PARAMS

const reducer = (state: State, action: ACTIONS) => {
  switch (action.type) {
    case ActionTypes.SET_QUERY: return { ...state, query: action.payload }
    case ActionTypes.SET_TYPE: return { ...state, type: action.payload }
    case ActionTypes.REPLACE_PARAMS: return action.payload
    default: return state
  }
}
type REPLACE_PARAMS = { type: ActionTypes.REPLACE_PARAMS, payload: State }
type SET_QUERY = { type: ActionTypes.SET_QUERY, payload: string }
type SET_TYPE = { type: ActionTypes.SET_TYPE, payload: 'album' | 'track' }

const setQuery = (payload: string): SET_QUERY => ({ type: ActionTypes.SET_QUERY, payload })
const replaceParams = (payload: State): REPLACE_PARAMS => ({ type: ActionTypes.REPLACE_PARAMS, payload })
const setType = (payload: string): SET_TYPE => {
  if (payload !== 'album' && payload !== 'track') { throw 'Bad Type' }
  return ({ type: ActionTypes.SET_TYPE, payload })
}

// =================


const MusicSearchBox = ({ onSearch, params: parentParams }: Props) => {
  const [state, dispatch] = useReducer(reducer, parentParams)
  const isFirst = useRef(true)

  useEffect(() => {
    // Avoid effect on first render, unlock for next renders:
    if (isFirst.current) { isFirst.current = false; return; }

    const handler = setTimeout(() => {
      onSearch(state)
    }, 500)

    return () => clearTimeout(handler)
  }, [state])

  useEffect(() => {
    isFirst.current = true
    dispatch(replaceParams(parentParams))
  }, [parentParams])


  return (<div className="input-group mb-3">

    <input type="text" className="form-control" placeholder="Search"
      value={state.query}
      onChange={e => dispatch(setQuery(e.target.value))} />

    <select value={state.type} onChange={e => dispatch(setType(e.target.value))}>
      <option value="album">Album</option>
      <option value="track">Track</option>
    </select>

    <div className="input-group-append">
      <button className="btn btn-outline-secondary" onClick={() => onSearch(state)}>
        Search
      </button>
    </div>


  </div>
  )
}

export default MusicSearchBox

