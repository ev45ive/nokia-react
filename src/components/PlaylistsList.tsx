import React, { useState } from 'react'
import { Playlist } from '../models/Playlist'

interface Props {
  someOptionFromParent?: boolean
  playlists: Playlist[],
  selected?: Playlist
  onSelect(selected: Playlist): void
}

const PlaylistsList = ({ playlists, selected, onSelect }: Props) => {

  return (
    <div>
      <div className="list-group">
        {playlists.map((playlist, index) =>
          <div
            className={`list-group-item ${
              selected?.id == playlist.id ? 'active' : ''
              }`}
            onClick={() => onSelect(playlist)}
            key={playlist.id}>

            {index + 1}. {playlist.name}
          </div>
        )}
      </div>
    </div>
  )
}

export default PlaylistsList
