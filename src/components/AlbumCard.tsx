import React from 'react'

interface Album {
  name: string
  images: {
    url: string
  }[]
}

interface Props {
  album: Album,
  onSelect(album: Album): void
}

const AlbumCard = ({ album, onSelect }: Props) => {
  return (
    <div className="card" onClick={() => onSelect(album)}>
      {album.images?.length ? 
      <img src={album.images[0].url} className="card-img-top" /> : <img src="https://www.placecage.com/c/200/300" className="card-img-top" />}

      <div className="card-body">
        <h5 className="card-title">{album.name}</h5>
        {/* 
        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        <a href="#" className="btn btn-primary">Go somewhere</a> */}
      </div>
    </div>
  )
}

export default AlbumCard
