import { Reducer, createStore, AnyAction, combineReducers, applyMiddleware, Middleware, MiddlewareAPI, Dispatch, compose, ReducersMapObject } from 'redux'
import { authReducer } from './modules/auth/auth.reducer';
import thunk from 'redux-thunk'
import { createBrowserHistory } from 'history'
import { connectRouter, routerMiddleware } from 'connected-react-router'

export const history = createBrowserHistory()

// const loadPlaylists = () => import('./modules/playlists/playlists.reducer')

export const configureReducers = (extra: ReducersMapObject<any, any> = {}) => combineReducers({
  router: connectRouter(history),
  auth: authReducer,
  // playlists: playlistsReducer,
  // search: searchReducer,
  ...extra
});


const composeEnhancers =
  typeof window === 'object' &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;


export const store = createStore(configureReducers(), composeEnhancers(
  applyMiddleware(
    routerMiddleware(history),
    thunk,
    // logger
  ),
  // other store enhancers if any
))

  ; (window as any).store = store;

  // A.handle(action)
  // A.next = B 
  // B.next = C
  // C.next = handleStoraACtion

  // https://github.com/supasate/connected-react-router
  // https://ngrx.io/guide/store/actions#writing-actions
  // https://github.com/zalmoxisus/redux-devtools-extension
  // https://www.npmjs.com/package/redux-actions
  // https://redux-saga.js.org/
  // https://github.com/redux-utilities/redux-promise