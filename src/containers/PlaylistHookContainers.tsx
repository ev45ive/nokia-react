import { FC, useEffect } from "react";
import PlaylistsDetails from "../components/PlaylistsDetails";
import { useStore, useSelector, useDispatch } from "react-redux";
import { playlistsSelectSelected, playlistUpdated, playlistSaveChanges, playlistSelected } from "../modules/playlists/playlists.reducer";
import React from "react";
import { push } from "connected-react-router";
import PlaylistsForm from "../components/PlaylistsForm";
import { Playlist, PlaylistPayload } from "../models/Playlist";
import { useParams } from "react-router-dom";
import { fetchPlaylistById } from "../services/search.service";



export const PlaylistSelectedDetails: FC<{ placki?: string }> = () => {
  // const {getState,dispatch} = useStore()
  const playlist = useSelector(playlistsSelectSelected);
  // const playlist = useSelector(playlistsSelectById(id));
  const dispatch = useDispatch()

  const { id } = useParams()

  useEffect(() => {
    if (playlist?.id === id) { return }

    fetchPlaylistById(id).then(playlist => {
      dispatch(playlistUpdated(playlist))
      dispatch(playlistSelected(playlist.id))
    })
  }, [id])


  return playlist && <PlaylistsDetails onEditClicked={() => {
    dispatch(push('/playlists/' + playlist.id + '/edit'))
  }} playlist={playlist} /> || null
}


export const PlaylistSelectedForm: FC<{}> = () => {

  // const {getState,dispatch} = useStore()

  const playlist = useSelector(playlistsSelectSelected);
  const dispatch = useDispatch()

  return playlist ? <PlaylistsForm
    playlist={playlist}
    onCancel={() => dispatch(push('/playlists/' + playlist.id))}
    onSave={(draft: any) => dispatch(playlistSaveChanges(playlist.id, draft))}
  /> : null
}