import React, { FC } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import { PlaylistPayload } from '../models/Playlist'
import { useDispatch } from 'react-redux'
import { createPlaylist } from '../modules/playlists/playlists.reducer'

interface Props {

}

const CreatePlaylistForm = (props: Props) => {
  const dispatch = useDispatch()

  const submit = (values: PlaylistPayload) => {
    dispatch(createPlaylist(values))
  }

  return (
    <div>
      <Formik
        // should reload when initial value changes
        enableReinitialize={false}
        initialValues={{
          name: '', collaborative: false, public: false, description: ''
        }}
        validate={(values) => {
          const errors: Record<string, string> = {}

          if (values.name.length < 3) {
            errors.name = 'Name too short'
          }

          return errors
        }}
        onSubmit={submit} >
        {({ submitForm, errors, touched }) => <Form>

          <div className="form-group">
            <label>Name:</label>
            <Field name="name" className="form-control" />
            <ErrorMessage name="name" />
          </div>

          <div className="form-group">
            <label>
              <Field name="collaborative" type="checkbox" />
              collaborative
            </label>

            <label>
              <Field name="public" type="checkbox" />
              public
            </label>

            <ErrorMessage name="collaborative" />
            <ErrorMessage name="public" />
          </div>


          <div className="form-group">
            <label>Description:</label>
            <Field as="textarea" name="description" className="form-control" />
            <ErrorMessage name="description" />
          </div>

          <input type="submit" value="Create" className="btn btn-success" />

        </Form>}
      </Formik>

    </div >
  )
}

export default CreatePlaylistForm

type SuperInputProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
> & { placki: string };

const SuperInput: FC<SuperInputProps> = ({ placki, ...rest }) => {

  return <input {...rest} placeholder={placki} />
}