import React, { FC, useEffect } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import { PlaylistPayload } from '../models/Playlist'
import { useDispatch, useSelector } from 'react-redux'
import { createPlaylist, playlistsSelectSelected, playlistSaveChanges, playlistUpdated, playlistSelected } from '../modules/playlists/playlists.reducer'
import { useParams } from 'react-router-dom'
import { fetchPlaylistById } from '../services/search.service'
import { push } from 'connected-react-router'

interface Props {

}

const EditPlaylistForm = (props: Props) => {
  const dispatch = useDispatch()
  const playlist = useSelector(playlistsSelectSelected)
  const { id } = useParams()

  useEffect(() => {
    if (playlist?.id === id) { return }

    fetchPlaylistById(id).then(playlist => {
      dispatch(playlistUpdated(playlist))
      dispatch(playlistSelected(playlist.id))
    })
  }, [id])


  if (!playlist) {
    return <p> Loading ...</p>
  }

  const submit = (values: PlaylistPayload) => {
    dispatch(playlistSaveChanges(playlist.id, values))
    dispatch(push('/playlists/' + id ))// + '/edit'))
  }

  return (
    <div>
      <Formik
        // should reload when initial value changes
        enableReinitialize={false}
        initialValues={{
          name: playlist.name,
          collaborative: playlist.collaborative!,
          public: playlist.public!,
          description: playlist.description!
        }}
        validate={(values) => {
          const errors: Record<string, string> = {}

          if (values.name.length < 3) {
            errors.name = 'Name too short'
          }

          return errors
        }}
        onSubmit={submit} >
        {({ submitForm, errors, touched }) => <Form>

          <div className="form-group">
            <label>Name:</label>
            <Field name="name" className="form-control" />
            <ErrorMessage name="name" />
          </div>

          <div className="form-group">
            <label>
              <Field name="collaborative" type="checkbox" />
              collaborative
            </label>

            <label>
              <Field name="public" type="checkbox" />
              public
            </label>

            <ErrorMessage name="collaborative" />
            <ErrorMessage name="public" />
          </div>


          <div className="form-group">
            <label>Description:</label>
            <Field as="textarea" name="description" className="form-control" />
            <ErrorMessage name="description" />
          </div>

          <input type="submit" value="Save" className="btn btn-success" />

        </Form>}
      </Formik>

    </div >
  )
}

export default EditPlaylistForm
