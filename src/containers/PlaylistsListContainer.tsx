import PlaylistsList from "../components/PlaylistsList";
import { connect, MapStateToPropsParam, MapDispatchToPropsParam, MergeProps } from 'react-redux'
import { Playlist } from "../models/Playlist";
import { playlistSelected, playlistsSelectList, playlistsSelectSelected, fetchAndSelectPlaylist } from "../modules/playlists/playlists.reducer";
import { bindActionCreators } from "redux";
import PlaylistsDetails from "../components/PlaylistsDetails";
import PlaylistsForm from "../components/PlaylistsForm";
import { withRouter } from "react-router-dom";
import { push } from "connected-react-router";


export const PlaylistsListContainer = connect((state, ownProps: { placki?: string }) => ({
  playlists: playlistsSelectList(state),
  selected: playlistsSelectSelected(state)
  // selected: playlistsSelectById(ownProps.match.params['id'], ownProps.placki)
}), {
  onSelect({ id }: Playlist) { return fetchAndSelectPlaylist(id) }
})(PlaylistsList)

export const withSelectedPlaylist = connect((state) => ({
  playlist: playlistsSelectSelected(state)
}), {
  onEditClicked() { return push('/playlists') },
  onSave(draft: Playlist) { },
  onCancel() { }
})

// export const PlaylistSelectedForm = withSelectedPlaylist(PlaylistsForm)
export const PlaylistSelectedDetails = withSelectedPlaylist(PlaylistsDetails)

// withRouter(withSelectedPlaylist((PlaylistsDetails)))