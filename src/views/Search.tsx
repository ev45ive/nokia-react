import React, { useState, useEffect, FC } from 'react'
import AlbumCard from '../components/AlbumCard'
import { Album, Track } from '../models/Album'
import MusicSearchBox from '../components/MusicSearchBox'
import { searchAlbums, searchByType } from '../services/search.service'
import { TrackListItem } from '../components/TrackListItem'
import { RouteComponentProps, useHistory, useLocation, useRouteMatch, useParams } from 'react-router-dom'

interface Props extends RouteComponentProps<{
  paramName: string
}> {

}

// placki123@placki.com
// placki123

const Search: FC<Props> = ({ history, location, match }) => {
  // (match.params as any)['paramName']
  // match.params['paramName']

  // const {push,replace} = useHistory()
  // const {pathname} = useLocation()
  // const {path} = useRouteMatch()
  // path == routing.home.path
  // const { playlist_id } = useParams()

  const [params, setParams] = useState({ query: '', type: 'album' })
  const [error, setError] = useState<Error | null>(null)
  const [results, setResults] = useState<Album[] | Track[]>([])

  useEffect(() => {
    // console.log(location.state)
    const queryParams = new URLSearchParams(location.search)
    setParams({
      ...params,
      type: queryParams.get('type') || 'album',
      query: queryParams.get('query') || ''
    })
  }, [location.search])

  useEffect(() => {
    setError(null)
    setResults([])
    searchByType(params)
      .then(setResults)
      .catch(err => setError(err))
  }, [params])

  const search = (params: { query: string, type: string }) => {
    // history.push({
    history.replace({
      // pathname:'/search'
      pathname: location.pathname,
      // search: params.query ? '?q=' + params.query : ''
      search: '?' + new URLSearchParams(params),
      state: {
        extra_params_invisible: 123
      }
    })
  }

  // const {push,replace} = useHistory()
  const select = ({ id }: Album) => {
    history.push('/albums/' + id)
  }


  return (
    <div>
      <div className="row">
        <div className="col">
          <MusicSearchBox params={params} onSearch={search}></MusicSearchBox>
        </div>
      </div>
      <div className="row">
        <div className="col">
          {error?.message}
          <div className="row">
            {params.type == 'album' && (results as Album[]).map((result) =>
              <div className="col-3" key={result.id}>
                <AlbumCard album={result} onSelect={select}></AlbumCard>
              </div>
            )}
            {params.type == 'track' && <div className="list-group">{(results as Track[]).map((track) =>
              <TrackListItem track={track} key={track.id} />
            )}</div>}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Search

