// tsrafce
import React, { ErrorInfo, useState, Fragment, useCallback, useMemo, useEffect } from 'react'
import PlaylistsList from '../components/PlaylistsList'
import PlaylistsDetails from '../components/PlaylistsDetails'
import PlaylistsForm from '../components/PlaylistsForm'
import { Playlist } from '../models/Playlist'
import { Route, useHistory, useRouteMatch, useParams } from 'react-router-dom'


interface Props {

}

const playlists: Playlist[] = [
  {
    type: 'Playlist',
    id: '123',
    name: 'Test Z listy',
    favorite: true,
    color: 'red',
  },
  {
    type: 'Playlist',
    id: '234',
    name: 'Test 234',
    favorite: true,
    color: 'red',
  },
  {
    type: 'Playlist',
    id: '345',
    name: 'Test 345',
    favorite: true,
    color: 'red',
  }
]

const Playlists = (props: Props) => {



  const [filter, setFilter] = useState('')
  const [selected, setSelected] = useState<Playlist | undefined>(playlists[1])

  const { params, url, path } = useRouteMatch()
  const { push } = useHistory()
  const { playlist_id } = useParams()

  useEffect(() => {
    setSelected(playlists.find(p => p.id == playlist_id))
  }, [playlist_id])

  const save = useCallback((draft: Playlist) => { console.log(draft) }, [])

  const edit = useCallback(() => push('/playlists/' + playlist_id + '/edit'), [playlist_id])
  const cancel = useCallback(() => push('/playlists/' + playlist_id), [playlist_id])
  const select = (id: Playlist['id']) => { push('/playlists/' + id) }

  const filtered = useMemo(() => playlists.filter(p =>
    p.name.toLocaleLowerCase().includes(filter.toLocaleLowerCase())), [filter])


  return (
    <div>
      <h1>Playlists</h1>

      <div className="row">
        <div className="col">
          <input type="text" className="form-control" onChange={e => setFilter(e.target.value)} />

          <PlaylistsList
            onSelect={p => select(p.id)}
            playlists={filtered}
            selected={selected} />
        </div>
        <div className="col">

          <Route path={path} exact={true}
            render={({ match, location, history }) => {
              return <>
                <PlaylistsDetails
                  playlist={selected}
                  onEditClicked={edit} />
              </>
            }} />

          <Route path={path + "/edit"} exact={true} render={
            () => selected && <PlaylistsForm
              playlist={selected}
              onSave={save}
              onCancel={cancel} />} />

        </div>
      </div>
    </div>
  )
}

export default Playlists


export class ErrorBoundary extends React.Component {
  state = {
    hasError: false
  }

  static getDerivedStateFromError(error: Error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    // You can also log the error to an error reporting service
    console.error(error, errorInfo);
  }

  render() {
    return this.state.hasError ? <h3>Error</h3> : this.props.children
  }
}