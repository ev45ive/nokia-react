import React, { useEffect, useState, useCallback, useRef } from 'react'
import AlbumCard from '../components/AlbumCard'
import { TrackListItem } from '../components/TrackListItem'
import { RouteComponentProps, useParams } from 'react-router-dom'
import { fetchAlbumById } from '../services/search.service'
import { Album, Track } from '../models/Album'
import { useDispatch } from 'react-redux'
import { addTrackToSelectedPlaylist } from '../modules/playlists/playlists.reducer'

interface Props /* extends RouteComponentProps<any> */ { }

const AlbumDetails = (props: Props) => {
  const [album, setAlbum] = useState<Album | null>(null)
  const [track, setTrack] = useState<Track | null>(null)
  const { album_id } = useParams()
  const playerRef = useRef<HTMLAudioElement>(null)
  const dispatch = useDispatch()

  useEffect(() => {
    fetchAlbumById(album_id).then(album => {
      setAlbum(album)
    })
  }, [album_id])

  const playTrack = useCallback((track: Track) => {
    setTrack(track)
  }, [])
  
  const onAddToPlaylist = useCallback((track: Track) => {
    dispatch(addTrackToSelectedPlaylist(track))
  }, [])

  useEffect(() => {
    if (playerRef.current) {
      playerRef.current.volume = 0.2;
      playerRef.current.play()
    }
  }, [track])


  if (!album) { return <p>Loading ...</p> }

  return (
    <div>
      <div className="row">
        <div className="col">
          <AlbumCard album={album} onSelect={() => { }} />
        </div>
        <div className="col">
          <audio controls={true} src={track?.preview_url} ref={playerRef}
            className="w-100 mb-3" />

          <div className="list-group">
            {
              album.tracks?.items.map(track =>
                <TrackListItem track={track} key={track.id} onPlay={playTrack} onAddToPlaylist={onAddToPlaylist} />)
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default AlbumDetails
