
import React, { useEffect } from 'react'
import { Route, useHistory } from 'react-router-dom'
import { PlaylistSelectedDetails, PlaylistSelectedForm } from '../containers/PlaylistHookContainers'
import { PlaylistsListContainer } from '../containers/PlaylistsListContainer'
import CreatePlaylistForm from '../containers/CreatePlaylistForm'
import { useDispatch } from 'react-redux'
import { fetchPlaylists } from '../modules/playlists/playlists.reducer'
import EditPlaylistForm from '../containers/EditPlaylistForm'

import { playlistsReducer } from '../modules/playlists/playlists.reducer';
import { configureReducers, store } from '../store'

store.replaceReducer(configureReducers({
  playlists: playlistsReducer
}));
store.dispatch({ type: 'PLAYLISTS_REDUCER_LOADED' })



interface Props {

}

const PlaylistsRedux = (props: Props) => {

  const { push } = useHistory()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchPlaylists())
  }, [])

  return (
    <div>
      <div className="row">
        <div className="col">
          <PlaylistsListContainer placki="123" />

          <button onClick={() => push('/playlists/create')}>Create new playlist</button>
        </div>
        <div className="col">
          {/* <RouteWithAction component={PlaylistSelectedDetails} resolve={} /> */}

          <Route path="/playlists/create" exact={true} component={CreatePlaylistForm} />
          <Route path="/playlists/:id" exact={true} component={PlaylistSelectedDetails} />
          <Route path="/playlists/:id/edit" exact={true} component={EditPlaylistForm} />

        </div>
      </div>
    </div>
  )
}

export default PlaylistsRedux

// /:a/:b/:c?kwarg
// def abc(a,b,c,*kwarg)