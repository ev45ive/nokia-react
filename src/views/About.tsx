import React, { useState, useEffect, Suspense } from 'react'

// const ReadMore = (path = 'MoreInfoAbout') => React.lazy(() => import('../containers/' + path))
const ReadMore = React.lazy(() => import('../containers/MoreInfoAbout'))

interface Props {

}

const About = (props: Props) => {
  const [readMore, setReadMore] = useState(false)

  useEffect(() => {

  })

  return (
    <div>
      <h1>About as</h1>

      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur blanditiis itaque est mollitia eligendi ullam molestias recusandae consectetur? Blanditiis veritatis accusamus, corrupti error ipsa ipsam soluta doloremque temporibus explicabo quaerat!</p>

      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur blanditiis itaque est mollitia eligendi ullam molestias recusandae consectetur? Blanditiis veritatis accusamus, corrupti error ipsa ipsam soluta doloremque temporibus explicabo quaerat!</p>

      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur blanditiis itaque est mollitia eligendi ullam molestias recusandae consectetur? Blanditiis veritatis accusamus, corrupti error ipsa ipsam soluta doloremque temporibus explicabo quaerat!</p>

      {readMore || <button onClick={() => setReadMore(true)}>Read more</button>}

      {readMore && <Suspense fallback={<p>Loading...</p>}>
        <ReadMore />
        <hr />
        <ReadMore />
        <hr />
        <ReadMore />
      </Suspense>}

    </div>
  )
}

export default About
