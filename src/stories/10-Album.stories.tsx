import React from 'react';
import { action } from '@storybook/addon-actions';
import { Button } from '@storybook/react/demo';
import { text, withKnobs } from '@storybook/addon-knobs';

import 'bootstrap/dist/css/bootstrap.css'
import AlbumCard from '../components/AlbumCard';

export default {
  title: 'Album',
  component: AlbumCard,
  decorators: [withKnobs]
};


export const AlbumCards = () => {
  const album = {
    name: text('Album name', 'Test Album'),
    images: [{
      url: text('Album image', 'https://www.placecage.com/c/300/300')
    }]
  }
  return (
    <div className="text-center mt-3" >
      <div className="mx-auto col-3">
        <AlbumCard album={album} onSelect={action('Album Select',{})} />
      </div>
    </div >
  );

}