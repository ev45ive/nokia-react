import React from 'react';
import { action } from '@storybook/addon-actions';
import { Button } from '@storybook/react/demo';
import { text, withKnobs } from '@storybook/addon-knobs';

import 'bootstrap/dist/css/bootstrap.css'
import AlbumCard from '../components/AlbumCard';
import SearchBox from '../components/SearchBox';

export default {
  title: 'Search Field',
  component: SearchBox,
  decorators: [withKnobs]
};


export const SearchField = () => <div className="m-3">

  <SearchBox onSearch={action('Search')}
             query={text('Query', 'batman')} />
</div>


export const TypeaheadSearchField = () => <div className="m-3">

  <SearchBox onSearch={action('Search')}
             query={text('Query', 'batman')} />
</div>