# GIT
git clone https://bitbucket.org/ev45ive/nokia-react
cd nokia-react
npm i 

npm start

# Update

git stash
git pull -f 


# Softrware
node -v 
npm -v 
code -v 
git --version

# Create react app

npm i -g create-react-app
create-react-app nokia-react --template=typescript

npx create-react-app nokia-react --template=typescript

# Proxy
code ~/.npmrc
proxy=adres_do_proxy
>npm config set https-proxy [tu-adres]

# Start
npm run start
npm start

# Styles
https://www.npmjs.com/package/typescript-plugin-css-modules
https://marketplace.visualstudio.com/items?itemName=jpoissonnier.vscode-styled-components

https://storybook.js.org/
https://styled-components.com/docs/basics#coming-from-css
https://emotion.sh/docs/theming
https://theme-ui.com/getting-started/#sx-prop

https://getbootstrap.com/
https://reactstrap.github.io/components/layout/#app

# Emmet
https://docs.emmet.io/cheat-sheet/

#main.container>.row.top-tow>.col*2^.row>.col{Test}
Container>Row>Col*2


# Snippets
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

https://code.visualstudio.com/docs/editor/userdefinedsnippets#_project-snippet-scope


# Linter + Formatter
https://github.com/prettier/eslint-config-prettier
https://thomlom.dev/setup-eslint-prettier-react/
https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint


# React classnames:
https://www.npmjs.com/package/classnames

# Storybook
npx -p @storybook/cli sb init --type react_scripts

// .storybook/main.js
.js => .tsx

yarn add @storybook/addon-storysource --dev
yarn add @storybook/addon-knobs --dev

https://marketplace.visualstudio.com/items?itemName=EQuimper.react-native-react-redux

# Paste JSON as code
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

Ctrl+Shift+P => Paste JSON

Ctrl+Shift+Alt+V
